public class MelonMain
{

	public static boolean runSandbox = false;

	public static void main(String args[]) throws InterruptedException
	{

		SaveClass Save = new SaveClass();

		LevelSelection l = new LevelSelection();

		while (LevelSelection.selection.equalsIgnoreCase(""))
		{

			Thread.sleep(1);

		}

		if (LevelSelection.selection.equalsIgnoreCase("Load")
				&& !LevelSelection.selection.equalsIgnoreCase("Sandbox"))
		{

			try
			{

				SaveClass.OpenFile();
				SaveClass.ReadFile();
				SaveClass.CloseFile();

				System.out.println("Loaded Save File");

			}

			catch (Exception e) {

				System.out.println("No save file exits, creating new...");

			}

		}

		if (LevelSelection.selection.equalsIgnoreCase("New")
				&& !LevelSelection.selection.equalsIgnoreCase("Sandbox")
				&& OptionScreen.enableSaves == true)
		{

			System.out.println("Automatic Saving is enabled");

			try
			{

				SaveClass.CreateFile();
				SaveClass.CloseFile();

			}

			catch (Exception e)
			{

				System.out.println("Unable to create new save file");

			}

		}

		if (LevelSelection.selection.equalsIgnoreCase("Sandbox")) {

			SandboxOptions s = new SandboxOptions();
			runSandbox = true;

		}

		while (runSandbox)
		{

			Thread.sleep(1);

		}

		Thread t = new Thread()
		{

			public void run()
			{

				boolean runThread = true;

				while (runThread)
				{

					if (!LevelSelection.selection.equalsIgnoreCase("Sandbox")
							&& OptionScreen.enableSaves == true)
					{

						runThread = true;

					}

					if (LevelSelection.selection.equalsIgnoreCase("Sandbox") || OptionScreen.enableSaves == false)
					{

						runThread = false;

					}

					int sTime = OptionScreen.interval * 1000;

					try
					{

						sleep(sTime);

					}

					catch (InterruptedException e)
					{

						e.printStackTrace();

					}

					SaveClass.CreateFile();
					SaveClass.AddRecords();
					SaveClass.CloseFile();

					System.out.println("Game Progress Saved");

				}

			}

		};

		t.start();

		long displayedNum = Format.numberMelons;

		Format f = new Format();

		while (Format.run)
		{

			Thread.sleep(1000);

			Format.numberMelons = Format.numberMelons + (Format.MPS);
			displayedNum = Format.numberMelons;

			Format.number = displayedNum + " Melons | " + Format.MPS + " MpS";
			Format.countMelon.setText(Format.number);

		}

	}

}
