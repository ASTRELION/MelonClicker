import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

public class OptionScreen extends JFrame
{
	
	private JLabel prompt;
	private JCheckBox autoSave;
	private JTextField saveInterval; 
	private JButton close;
	
	public static boolean enableSaves = false;
	public static int interval;
	public static boolean sRun = false;
	
	public OptionScreen() //automatic saves, save interval
	{
		
		super("Options");
		
		prompt = new JLabel("Melon Clicker Options");
		
		autoSave = new JCheckBox("Enable Automatic Saves");
		autoSave.setToolTipText("Check if you want to enable Melon Clicker to automatically save for you");
		add (autoSave, BorderLayout.NORTH);
		
		saveInterval = new JTextField(5);
		saveInterval.setToolTipText("Set the interval of automatic saving (in seconds). 'Enable Automatic Saves' for this to function");
		saveInterval.setEditable(false);
		add (saveInterval, BorderLayout.NORTH);
		
		close = new JButton("Done");
		add (close, BorderLayout.SOUTH);

		autoSaveClass saveC = new autoSaveClass();
		saveIntervalClass intervalC = new saveIntervalClass();
		closeClass closeC = new closeClass();
		
		autoSave.addActionListener(saveC);
		saveInterval.addActionListener(intervalC);
		close.addActionListener(closeC);
		
		setLayout(new FlowLayout());
		setSize(300,100);
		setVisible(true);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
		
	}
	
	private class autoSaveClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event)
		{
			
			if (autoSave.isSelected())
			{
				
				enableSaves = true;
				saveInterval.setEditable(true);
				System.out.println("Automatic Saving is now enabled");
				
			}
			
			if (!autoSave.isSelected())
			{
				
				enableSaves = false;
				saveInterval.setEditable(false);
				saveInterval.setText("");
				System.out.println("Automatic Saving is now disabled");
				
			}
			
		}
		
	} 
	
	private class saveIntervalClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event)
		{
			
			if (event.getSource() == saveInterval)
			{
				
				sRun = true;
				
				String intervalS;
				intervalS = event.getActionCommand();
				
				try
				{
				
					interval = Integer.parseInt(intervalS);
					
					System.out.println("Saving interval set to " + interval + " seconds");
				
				}
				
				catch(Exception e)
				{
					
					System.out.println("That is not a valid integer");
					
				}
				
				sRun = false;
				
			}
			
		}
		
	} 
	
	private class closeClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event)
		{
			
			System.out.println("Closing option screen...");
			dispose();
			
		}
		
	} 
	
}
