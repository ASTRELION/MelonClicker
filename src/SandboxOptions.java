import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class SandboxOptions extends JFrame
{
	
	private JLabel directions;
	private JButton confirm;
	private JTextField clickerP, slaveP, farmP, factoryP, plantP, plantationP, pumpkinP, antimatterP;
	private JTextField clickerM, slaveM, farmM, factoryM, plantM, plantationM, pumpkinM, antimatterM;
	
	public static int clickerbase, slavebase, farmbase, factorybase, plantbase, plantationbase, pumpkinbase, antimatterbase;
	public static int clickerm, slavem, farmm, factorym, plantm, plantationm, pumpkinm, antimatterm;
	
	public SandboxOptions()
	{
		
		super("Sandbox Options");
		
		JPanel e = new JPanel();
		JPanel e2 = new JPanel();
		JPanel e3 = new JPanel();
		JPanel t = new JPanel();
		JPanel b = new JPanel();
		
		directions = new JLabel("Enter integers into the following prompts. Keep values below 1 million to reduce errors.");
		t.add(directions, BorderLayout.NORTH);
		
		//Clicker
		clickerP = new JTextField(5);
		clickerM = new JTextField(5);
		clickerP.setToolTipText("Clicker Base Price");
		clickerM.setToolTipText("Clicker MpS");
		e.add(clickerP);
		e.add(clickerM);
		
		//Slave
		slaveP = new JTextField(5);
		slaveM = new JTextField(5);
		slaveP.setToolTipText("Slave Base Price");
		slaveM.setToolTipText("Slave MpS");
		e.add(slaveP);
		e.add(slaveM);
		
		//Farm
		farmP = new JTextField(5);
		farmM = new JTextField(5);
		farmP.setToolTipText("Farm Base Price");
		farmM.setToolTipText("Farm MpS");
		e.add(farmP);
		e.add(farmM);
		
		//Factory
		factoryP = new JTextField(5);
		factoryM = new JTextField(5);
		factoryP.setToolTipText("Factory Base Price");
		factoryM.setToolTipText("Factory MpS");
		e2.add(factoryP);
		e2.add(factoryM);
		
		//Plant
		plantP = new JTextField(5);
		plantM = new JTextField(5);
		plantP.setToolTipText("Steroid Plant Price");
		plantM.setToolTipText("Steroid Plant MpS");
		e2.add(plantP);
		e2.add(plantM);
		
		//Plantation
		plantationP = new JTextField(5);
		plantationM = new JTextField(5);
		plantationP.setToolTipText("Plantation Price");
		plantationM.setToolTipText("Plantation MpS");
		e2.add(plantationP);
		e2.add(plantationM);
		
		//Pumpkin Converter
		pumpkinP = new JTextField(5);
		pumpkinM = new JTextField(5);
		pumpkinP.setToolTipText("Pumpkin Converter Price");
		pumpkinM.setToolTipText("Pumpkin Converter MpS");
		e3.add(pumpkinP);
		e3.add(pumpkinM);
		
		//Anti-Matter Condenser
		antimatterP = new JTextField(5);
		antimatterM = new JTextField(5);
		antimatterP.setToolTipText("Antimatter Condenser Price");
		antimatterM.setToolTipText("Antimatter Condenser MpS");
		e3.add(antimatterP);
		e3.add(antimatterM);
		
		confirm = new JButton("                                        Play                                        ");
		confirm.setToolTipText("Un-entered Values will be 0");
		b.add(confirm, BorderLayout.CENTER);
		
		add(t, BorderLayout.NORTH);
		add(e, BorderLayout.SOUTH);
		add(e2, BorderLayout.SOUTH);
		add(e3, BorderLayout.SOUTH);
		add(b, BorderLayout.SOUTH);
		
		ButtonClass button = new ButtonClass();
		
		confirm.addActionListener(button);
		
		clickerP.addActionListener(button);
		clickerM.addActionListener(button);

		slaveP.addActionListener(button);
		slaveM.addActionListener(button);

		farmP.addActionListener(button);
		farmM.addActionListener(button);

		factoryP.addActionListener(button);
		factoryM.addActionListener(button);

		plantP.addActionListener(button);
		plantM.addActionListener(button);

		plantationP.addActionListener(button);
		plantationM.addActionListener(button);

		pumpkinP.addActionListener(button);
		pumpkinM.addActionListener(button);

		antimatterP.addActionListener(button);
		antimatterM.addActionListener(button);
		
		setLayout(new FlowLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(580,230);
		setVisible(true);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
		
	}
	
	private class ButtonClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event)
		{
			
			//clicker
			String ClickerBaseS = clickerP.getText();
			
			try
			{
				
			clickerbase = Integer.parseInt(ClickerBaseS);
			
			}
			catch (Exception e)
			{
				
		
				
			}
			System.out.println("Clicker base price set to " + clickerbase);
			
			String ClickerMS = clickerM.getText();
			try
			{
				
				clickerm = Integer.parseInt(ClickerMS);
				
			}
			catch (Exception e)
			{
				
	
				
			}
			System.out.println("Clicker MpS set to " + clickerm);
			
			//slave
			String SlaveBaseS = slaveP.getText();
			
			try
			{
				
				slavebase = Integer.parseInt(SlaveBaseS);
				
			}
			catch (Exception e)
			{
				
		
				
			}

			System.out.println("Slave base price set to " + slavebase);
			
			String SlaveMS = slaveM.getText();
			
			try
			{
				
				slavem = Integer.parseInt(SlaveMS);
				
			}
			catch (Exception e)
			{
				

				
			}

			System.out.println("Slave MpS set to " + slavem);
			
			//farm
			String FarmBaseS = farmP.getText();
			
			try
			{
				
				farmbase = Integer.parseInt(FarmBaseS);
				
			}
			catch (Exception e)
			{
				
		
				
			}

			System.out.println("Farm base price set to " + farmbase);
			
			String FarmMS = farmM.getText();
			
			try
			{
				
				farmm = Integer.parseInt(FarmMS);
				
			}
			catch (Exception e)
			{
				
	
				
			}

			System.out.println("Farm MpS set to " + farmm);
			
			//factory
			String FactoryS = factoryP.getText();
			
			try
			{
				
				factorybase = Integer.parseInt(FactoryS);
				
			}
			catch (Exception e)
			{
				
	
				
			}

			System.out.println("Factory base price set to " + factorybase);
			
			String FactoryMS = factoryM.getText();
			
			try
			{
				
				factorym = Integer.parseInt(FactoryMS);
				
			}
			catch (Exception e)
			{
				
	
				
			}

			System.out.println("Factory MpS set to " + factorym);
			
			//plant
			String PlantBaseS = plantP.getText();
			
			try
			{
				
				plantbase = Integer.parseInt(PlantBaseS);
				
			}
			catch (Exception e)
			{
				
	
				
			}

			System.out.println("Steroid Plant base price set to " + plantbase);
			
			String PlantMS = plantM.getText();
			
			try
			{
				
				plantm = Integer.parseInt(PlantMS);
				
			}
			catch (Exception e)
			{
				
	
				
			}

			System.out.println("Steroid Plant MpS set to " + plantm);
			
			//plantation
			String PlantationBaseS = plantationP.getText();
			
			try
			{
				
				plantationbase = Integer.parseInt(PlantationBaseS);
				
			}
			catch (Exception e)
			{
				
		
				
			}

			System.out.println("Plantation base price set to " + plantationbase);
			
			String PlantationMS = plantationM.getText();
			
			try
			{
				
				plantationm = Integer.parseInt(PlantationMS);
				
			}
			catch (Exception e)
			{
				

				
			}
			
			System.out.println("Plantation MpS set to " + plantationm);
			
			//pumpkin
			String PumpkinBaseS = pumpkinP.getText();
			
			try
			{
				
				pumpkinbase = Integer.parseInt(PumpkinBaseS);
				
			}
			catch (Exception e)
			{
				
			
				
			}
		
			System.out.println("Pumpkin Converter base price set to " + pumpkinbase);
			
			String PumpkinMS = pumpkinM.getText();
			
			try
			{
				
				pumpkinm = Integer.parseInt(PumpkinMS);
				
			}
			catch (Exception e)
			{
				
				
				
			}

			System.out.println("Pumpkin Converter MpS set to " + pumpkinm);
			
			//antimatter
			String AntimatterBaseS = antimatterP.getText();
			
			try
			{
				
				antimatterbase = Integer.parseInt(AntimatterBaseS);
				
			}
			catch (Exception e)
			{
				
				
				
			}

			System.out.println("Antimatter Condenser base price set to " + antimatterbase);
			
			String AntimatterMS = antimatterM.getText();
			
			try
			{
				
				antimatterm = Integer.parseInt(AntimatterMS);
				
			}
			catch (Exception e)
			{
				
				
				
			}

			System.out.println("Antimatter Condenser MpS set to " + antimatterm);
			
			MelonMain.runSandbox = false;
			
			dispose();
			
		}
		
	}
	
}
