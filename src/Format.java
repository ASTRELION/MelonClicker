import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Format extends JFrame
{
	
	public static String number;
	public static JLabel countMelon;
	public static JLabel t;
	private JButton clickMelon;
	public static long numberMelons;
	public static long MPS;
	public static boolean run = true;
	public static int time = 600;
	
	private JButton clicker, slave, farm, factory, steroidPlant, Plantation, PumpkinConverter, antimattercondenser;
	
	public static long ClickerOwned = 0, SlaveOwned = 0, FarmOwned = 0, FactoryOwned = 0, PlantOwned = 0, PlantationOwned = 0, PumpkinOwned = 0, AntimatterCondenserOwned = 0;
	
	//Base building prices//
	public static long ClickerBase = 15, SlaveBase = 100, FarmBase = 500, FactoryBase = 1500, PlantBase = 5000, PlantationBase = 7500, PumpkinconvertBase = 25000, AntimatterBase = 50000;
	
	//MpS//
	public static long ClickerM = 1, SlaveM = 5, FarmM = 15, FactoryM = 45, PlantM = 100, PlantationM = 225, PumpkinM = 350, AntimatterM = 500;
	
	public Format() throws InterruptedException
	{
		
		super("Melon Clicker");
		
		if (LevelSelection.selection.equalsIgnoreCase("Sandbox"))
		{
			
			SandboxOptions s = new SandboxOptions();
			
			ClickerBase = s.clickerbase;
			ClickerM = s.clickerm;
			
			SlaveBase = s.slavebase;
			SlaveM = s.slavem;
			
			FarmBase = s.farmbase;
			FarmM = s.farmm;
			
			FactoryBase = s.factorybase;
			FactoryM = s.factorym;
			
			PlantBase = s.plantbase;
			PlantM = s.plantm;
			
			PlantationBase = s.plantationbase;
			PlantationM = s.plantationm;
			
			PumpkinconvertBase = s.pumpkinbase;
			PumpkinM = s.pumpkinm;
			
			AntimatterBase = s.antimatterbase;
			AntimatterM = s.antimatterm;
			
		}
		
		JPanel cA = new JPanel(new GridBagLayout());
		JPanel bA = new JPanel(new GridBagLayout());
		
		GridBagConstraints clickGBC = new GridBagConstraints();
		GridBagConstraints buttonGBC = new GridBagConstraints();
		
		clickGBC.insets = new Insets(0,5,0,25);
		
		clickGBC.gridx = 0;
		clickGBC.gridy = 0;
		
		countMelon = new JLabel("Click!");
		cA.add(countMelon, clickGBC);
		
		clickGBC.gridx = 0;
		clickGBC.gridy = 1;
		
		buttonGBC.insets = new Insets(0,25,0,5);
		
		buttonGBC.gridx = 10;
		buttonGBC.gridy = 1;
		
		Icon melon = new ImageIcon(getClass().getResource("Melon_Slice.png"));
		clickMelon = new JButton(melon);
		clickMelon.setToolTipText("Click to get melons");
		cA.add(clickMelon, clickGBC);
		
		buttonGBC.gridx = 10;
		buttonGBC.gridy = 2;
		
		clicker = new JButton("Clicker | " + ClickerBase + "M");
		clicker.setToolTipText(ClickerM + "Mps Each");
		bA.add(clicker, buttonGBC);
		
		buttonGBC.gridx = 10;
		buttonGBC.gridy = 3;
		
		slave = new JButton("Slave | " + SlaveBase + "M");
		slave.setToolTipText(SlaveM + "MpS Each");
		bA.add(slave, buttonGBC);
		
		buttonGBC.gridx = 10;
		buttonGBC.gridy = 4;
		
		farm = new JButton("Farm | " + FarmBase + "M");
		farm.setToolTipText(FarmM + "MpS Each");
		bA.add(farm, buttonGBC);
		
		buttonGBC.gridx = 10;
		buttonGBC.gridy = 5;
		
		factory = new JButton("Factory | " + FactoryBase + "M");
		factory.setToolTipText(FactoryM + "MpS Each");
		bA.add(factory, buttonGBC);
		
		buttonGBC.gridx = 10;
		buttonGBC.gridy = 6;
		
		steroidPlant = new JButton("Steroid Plant | " + PlantBase + "M");
		steroidPlant.setToolTipText(PlantM + "MpS Each");
		bA.add(steroidPlant, buttonGBC);
		
		buttonGBC.gridx = 10;
		buttonGBC.gridy = 7;
		
		Plantation = new JButton("Plantation | " + PlantationBase + "M");
		Plantation.setToolTipText(PlantationM + "MpS Each");
		bA.add(Plantation, buttonGBC);
		
		buttonGBC.gridx = 10;
		buttonGBC.gridy = 8;
		
		PumpkinConverter = new JButton("Pumpkin Converter | " + PumpkinconvertBase + "M");
		PumpkinConverter.setToolTipText(PumpkinM + "MpS Each");
		bA.add(PumpkinConverter, buttonGBC);
		
		buttonGBC.gridx = 10;
		buttonGBC.gridy = 9;
		
		antimattercondenser = new JButton("Anti-Matter Condenser | " + AntimatterBase + "M");
		antimattercondenser.setToolTipText(AntimatterM + "MpS Each");
		bA.add(antimattercondenser, buttonGBC);
		
		add(cA, BorderLayout.WEST);
		add(bA, BorderLayout.EAST);
		
		HandlerClass handler = new HandlerClass();
		clickMelon.addActionListener(handler);
		
		clickerClass clickerC = new clickerClass();
		clicker.addActionListener(clickerC);
		
		slaveClass slaveC = new slaveClass();
		slave.addActionListener(slaveC);
		
		farmClass farmC = new farmClass();
		farm.addActionListener(farmC);
		
		factoryClass factoryC = new factoryClass();
		factory.addActionListener(factoryC);
		
		steroidplantClass steroidC = new steroidplantClass();
		steroidPlant.addActionListener(steroidC);
		
		plantationClass plantationC = new plantationClass();
		Plantation.addActionListener(plantationC);
		
		pumpkinconverterClass pumpkinC = new pumpkinconverterClass();
		PumpkinConverter.addActionListener(pumpkinC);
		
		antimattercondenserClass antimatterC = new antimattercondenserClass();
		antimattercondenser.addActionListener(antimatterC);
		
		setLayout(new FlowLayout());	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500,300);
		setVisible(true);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
		
	}
	
	private class HandlerClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event)
		{
			
			numberMelons++;
			number = numberMelons + " Melons | " + MPS + " MpS";
			countMelon.setText(number);
			
		}
		
	}
	
	private class clickerClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event) //1 a second
		{
			
			if (numberMelons < ClickerBase * (ClickerOwned + 1))
			{
				
				long left = (ClickerBase * (ClickerOwned + 1)) - numberMelons;
				JOptionPane.showMessageDialog(null, "You need " + left + " more melons for that!");
				
			}
			
			if (numberMelons >= ClickerBase * (ClickerOwned + 1))
			{
				
				long price = ClickerBase + (ClickerBase * (ClickerOwned + 1));
			
				numberMelons = numberMelons - (ClickerBase * (ClickerOwned + 1));
				
				ClickerOwned++;
				MPS = MPS + ClickerM;
				clicker.setText("Clicker | " + price + "M [" + ClickerOwned + "]");
			
			}
			
		}
		
	}
	
	private class slaveClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event) //7 a second
		{
			
			if (numberMelons < SlaveBase * (SlaveOwned + 1))
			{
				
				long left = (SlaveBase * (SlaveOwned + 1)) - numberMelons;
				JOptionPane.showMessageDialog(null, "You need " + left + " more melons for that!");
				
			}
			
			if (numberMelons >= SlaveBase * (SlaveOwned + 1))
			{
				
				long price = SlaveBase + (SlaveBase * (SlaveOwned + 1));
			
				numberMelons = numberMelons - (SlaveBase * (SlaveOwned + 1));
				
				SlaveOwned++;
				MPS = MPS + SlaveM;
				slave.setText("Slave | " + price + "M [" + SlaveOwned + "]");
			
			}
			
		}
		
	}
	
	private class farmClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event) //20 a second
		{
			
			if (numberMelons < FarmBase * (FarmOwned + 1))
			{
				
				long left = (FarmBase * (FarmOwned + 1)) - numberMelons;
				JOptionPane.showMessageDialog(null, "You need " + left + " more melons for that!");
				
			}
			
			if (numberMelons >= FarmBase * (FarmOwned + 1))
			{

				long price = FarmBase + (FarmBase * (FarmOwned + 1));
			
				numberMelons = numberMelons - (FarmBase * (FarmOwned + 1));
				
				FarmOwned++;
				MPS = MPS + FarmM;
				farm.setText("Farm | " + price + "M [" + FarmOwned + "]");
			
			}
			
		}
		
	}
	
	private class factoryClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event) //125 a second
		{
			
			if (numberMelons < FactoryBase * (FactoryOwned + 1))
			{
				
				long left = (FactoryBase * (FactoryOwned + 1)) - numberMelons;
				JOptionPane.showMessageDialog(null, "You need " + left + " more melons for that!");
				
			}
			
			if (numberMelons >= FactoryBase * (FactoryOwned + 1))
			{
				
				long price = FactoryBase + (FactoryBase * (FactoryOwned + 1));
			
				numberMelons = numberMelons - (FactoryBase * (FactoryOwned + 1));
				
				FactoryOwned++;
				MPS = MPS + FactoryM;
				factory.setText("Factory | " + price + "M [" + FactoryOwned + "]");
			
			}
			
		}
		
	}
	
	private class steroidplantClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event) //200 a second
		{
			
			if (numberMelons < PlantBase * (PlantOwned + 1))
			{
				
				long left = (PlantBase * (PlantOwned + 1)) - numberMelons;
				JOptionPane.showMessageDialog(null, "You need " + left + " more melons for that!");
				
			}
			
			if (numberMelons >= PlantBase * (PlantOwned + 1))
			{
				
				long price = PlantBase + (PlantBase * (PlantOwned + 1));
			
				numberMelons = numberMelons - (PlantBase * (PlantOwned + 1));
				
				PlantOwned++;
				MPS = MPS + PlantM;
				steroidPlant.setText("Steroid Plant | " + price + "M [" + PlantOwned + "]");
			
			}
			
		}
		
	}
	
	private class plantationClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event) //200 a second
		{
			
			if (numberMelons < (PlantationBase * (PlantationOwned + 1)))
			{
				
				long left = (PlantationBase * (PlantationOwned + 1)) - numberMelons;
				JOptionPane.showMessageDialog(null, "You need " + left + " more melons for that!");
				
			}
			
			if (numberMelons >= PlantationBase + (PlantationBase * (PlantationOwned + 1)))
			{
				
				long price = PlantationBase + (PlantationBase * (PlantationOwned + 1));
			
				numberMelons = numberMelons - PlantationBase * (PlantationOwned + 1);
				
				PlantationOwned++;
				MPS = MPS + PlantationM;
				Plantation.setText("Plantation | " + price + "M [" + PlantationOwned + "]");
			
			}
			
		}
		
	}
	
	private class pumpkinconverterClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event) //200 a second
		{
			
			if (numberMelons < PumpkinconvertBase * (PlantOwned + 1))
			{
				
				long left = (PumpkinconvertBase * (PumpkinOwned + 1)) - numberMelons;
				JOptionPane.showMessageDialog(null, "You need " + left + " more melons for that!");
				
			}
			
			if (numberMelons >= (PumpkinconvertBase * (PumpkinOwned + 1)))
			{
				
				long price = PumpkinconvertBase + (PumpkinconvertBase * (PumpkinOwned + 1));
			
				numberMelons = numberMelons - (PumpkinconvertBase * (PumpkinOwned + 1));
				
				PumpkinOwned++;
				MPS = MPS + PumpkinM;
				PumpkinConverter.setText("Pumpkin Converter | " + price + "M [" + PumpkinOwned + "]");
			
			}
			
		}
		
	}
	
	private class antimattercondenserClass implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event) //400 a second
		{
			
			if (numberMelons < AntimatterBase * (AntimatterCondenserOwned + 1))
			{
				
				long left = (AntimatterBase * (AntimatterCondenserOwned + 1)) - numberMelons;
				JOptionPane.showMessageDialog(null, "You need " + left + " more melons for that!");
				
			}
			
			if (numberMelons >= AntimatterBase * (AntimatterCondenserOwned + 1))
			{
				
				long price = AntimatterBase + (AntimatterBase * (AntimatterCondenserOwned + 1));
			
				numberMelons = numberMelons - (AntimatterBase * (AntimatterCondenserOwned + 1));
				
				AntimatterCondenserOwned++;
				MPS = MPS + AntimatterM;
				antimattercondenser.setText("Anti-Matter Condenser | " + price + "M [" + AntimatterCondenserOwned + "]");
			
			}
			
		}
		
	}
	
}
