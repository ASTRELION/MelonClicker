import java.io.File;
import java.util.Formatter;
import java.util.Scanner;

public class SaveClass
{
	
	private static Formatter form;
	public static Scanner reader;
	public static int Melons, MpS, Clicker, Slave, Farm, Factory, Plant, Plantation, Pumpkin, Antimatter;

	public static void CreateFile()
	{
	
		try
		{
			
			form = new Formatter("MelonClickerSaves.txt");
			System.out.println("Created new save file.");
			
		}
		
		catch(Exception e)
		{
			
			System.out.println("Already have file");
			
		}
		
	}
	
	public static void OpenFile()
	{
		
		try
		{
		
		reader = new Scanner(new File("MelonClickerSaves.txt"));
		
		}
		
		catch(Exception e)
		{
			
			System.out.println("Could not find save file.");
			
		}
		
	}
	
	public static void ReadFile()
	{
		
		while(reader.hasNext())
		{
			
			String MelonCount = reader.next();
			String MpSS = reader.next();
			String ClickerS = reader.next();
			String SlaveS = reader.next();
			String FarmS = reader.next();
			String FactoryS = reader.next();
			String PlantS = reader.next();
			String PlantationS = reader.next();
			String PumpkinCS = reader.next();
			String AntimatterCS = reader.next();
			
			Melons = Integer.parseInt(MelonCount);
			MpS = Integer.parseInt(MpSS);
			Clicker = Integer.parseInt(ClickerS);
			Slave = Integer.parseInt(SlaveS);
			Farm = Integer.parseInt(FarmS);
			Factory = Integer.parseInt(FactoryS);
			Plant = Integer.parseInt(PlantS);
			Plantation = Integer.parseInt(PlantationS);
			Pumpkin = Integer.parseInt(PumpkinCS);
			Antimatter = Integer.parseInt(AntimatterCS);
			
			Format.numberMelons = Melons;
			Format.MPS = MpS;
			Format.ClickerOwned = Clicker;
			Format.SlaveOwned = Slave;
			Format.FarmOwned = Farm;
			Format.FactoryOwned = Factory;
			Format.PlantOwned = Plant;
			Format.PlantationOwned = Plantation;
			Format.PumpkinOwned = Pumpkin;
			Format.AntimatterCondenserOwned = Antimatter;
			
		}
		
	}
	
	public static void AddRecords()
	{
		
		form.format("%o %o %o %o %o %o %o %o %o %o", Format.numberMelons, Format.MPS, Format.ClickerOwned, Format.SlaveOwned, Format.FarmOwned, Format.FactoryOwned, Format.PlantOwned, Format.PlantationOwned, Format.PumpkinOwned, Format.AntimatterCondenserOwned);
		
	}
	
	public static void CloseFile()
	{
		
		form.close();
		
	}
	
}
