import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

public class LevelSelection extends JFrame
{
	
	public static String selection = "";
	private JButton New, Load, Sandbox, Fixes, Credits, Options;
	
	public LevelSelection()
	{
		
		super("Main Menu");
		
		New = new JButton("New Game");
		add(New, BorderLayout.CENTER);
		New.setToolTipText("Play a new game of Melon Clicker");
		
		Load = new JButton("Load Game");
		add(Load, BorderLayout.CENTER);
		Load.setToolTipText("Load your previous save of Melon Clicker, if no save exists Melon Clicker will create one.");
		
		Sandbox = new JButton("Sandbox mode");
		add(Sandbox, BorderLayout.CENTER);
		Sandbox.setToolTipText("Choose your own values of anything in the game. "
				+ "\n(eg. MpS, prices, ect.)");
		
		Options = new JButton("Options");
		add(Options, BorderLayout.CENTER);
		Options.setToolTipText("Melon Clicker Options");
		
		Fixes = new JButton("Fixes and Additions");
		add(Fixes, BorderLayout.CENTER);
		Fixes.setToolTipText("Bug Fixes and Additions in this version of Melon Clicker");
		
		Credits = new JButton("Credits");
		add(Credits, BorderLayout.CENTER);
		Credits.setToolTipText("Credits");
		
		NewSelect NewClass = new NewSelect();
		New.addActionListener(NewClass);
		
		LoadSelect LoadClass = new LoadSelect();
		Load.addActionListener(LoadClass);
		
		SandboxSelect SandboxClass = new SandboxSelect();
		Sandbox.addActionListener(SandboxClass);
		
		FixesSelect FixesClass = new FixesSelect();
		Fixes.addActionListener(FixesClass);
		
		OptionsSelect OptionsClass = new OptionsSelect();
		Options.addActionListener(OptionsClass);
		
		CreditsSelect CreditsClass = new CreditsSelect();
		Credits.addActionListener(CreditsClass);
		
		setLayout(new FlowLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(175,235);
		setVisible(true);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
		
	}
	
	private class NewSelect implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event)
		{
			
			selection = "New";
			dispose();
			
		}
		
	}
	
	private class LoadSelect implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event)
		{
			
			selection = "Load";
			dispose();
			
		}
		
	}
	
	private class SandboxSelect implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event)
		{
			
			boolean run = true;
			
			selection = "Sandbox";
			
			dispose();
			
		}
		
	}
	
	private class FixesSelect implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event)
		{
			
			JOptionPane.showMessageDialog(null, "Fixes/Additions: "
					+ "\n*Fixed Sandbox mode"
					+ "\n*Made level save interval only enterable if enabled");
			
		}
		
	}
	
	private class OptionsSelect implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event)
		{
			
			OptionScreen o = new OptionScreen();
			
		}
		
		
	}
	
	private class CreditsSelect implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event)
		{
			
			JOptionPane.showMessageDialog(null, "Credits: "
					+ "\n\nAuthor: "
					+ "\nASTRELION. "
					+ "\n\nBased From: Cookier Clicker"
					+ "\n\nLines of Code: 1, 480");
			
		}
		
	}
	
}
